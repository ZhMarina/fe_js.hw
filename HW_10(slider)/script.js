

const left = document.querySelector("div.previous");
const right = document.querySelector("div.next");
const slides = document.querySelectorAll("img");
const indicatorDots = document.querySelector('div.slider-dots');

let currentNumber = 0; // Объявляем переменную i  


for (let j = 0; j < slides.length; j++) {    // создать  точки-индикаторы в соответствии от количества слайдов
    let dot = document.createElement("span")
    dot.className = 'classdot'

    dot.addEventListener("click", () => {    // навеесить событие на крапку-индикатор

        currentNumber = j    // записать в глобальную i новое значение (номер нажатой кнопки )
        changeSelectedPhoto(j)

    })

    indicatorDots.appendChild(dot) // добавить точку в тело документа 
};


let dots = document.getElementsByClassName('classdot'); // массив всех span елементов 
changeSelectedPhoto(currentNumber) // вывести первое фото ( i = 0) и первую закрашеную точку


right.addEventListener("click", () => {  // событие на кпонку next 
    ++currentNumber
    if (currentNumber == slides.length) {   // если i == 6, то при нажатие i = 0 и снова появится первая картинка
        currentNumber = 0;
    }
    changeSelectedPhoto(currentNumber)
})


left.addEventListener("click", () => {// событие на кпонку previous
    --currentNumber
    if (currentNumber < 0) {  // при нажатие i = 5 и  появится послядняя картинка
        currentNumber = slides.length - 1;
    }
    changeSelectedPhoto(currentNumber)
})


function changeSelectedPhoto(index) {
    for (let s = 0; s < dots.length; s++) { // перебрать массив крапок и все сделать нейтральным цветом
        dots[s].style.backgroundColor = index === s ? '#000000' :'#BBB'  //  сделать нажатую кнопку или кнопку которая соотвествует картинку другим цветом
    }


    for (let m = 0; m < slides.length; m++) { // очистить поле от всех картинок
        slides[m].style.display = index === m ? 'block' : 'none';
    }
}