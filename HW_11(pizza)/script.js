// dragstart — происходит, когда пользователь начинает перетаскивать элемент.
// drag — происходит, когда элемент перетаскивается.
// dragend — происходит, когда пользователь закончил перетаскивание элемента.
// dragenter — происходит, когда перетаскиваемый элемент попадает в целевой объект.
// dragleave — происходит, когда перетаскиваемый элемент покидает целевой объект.
// dragover — происходит, когда перетаскиваемый элемент находится над целью.
// drop — происходит, когда перетаскиваемый элемент падает на целевой объект.

const   images = document.querySelectorAll('.draggable'),
        ingredients = document.querySelectorAll('.ingridients'),
        basis = document.querySelector('.table'),
        radio = document.getElementsByName('size'),
        dropTarget = document.querySelector('.pizza'),
        totalPrice = document.querySelector('.price'),
        pricePizza = document.createElement('p'),
        saucess = document.querySelector('.sauces'),
        tosping = document.querySelector('.tosping'),
        allSauces = document.querySelector('.sauces'),
        allToppings = document.querySelector('.topings'),
        runButton = document.querySelector('#banner');

let     toppings = [],
        sauces = [],
        priceBasis = 0,
        additivesPrice = 0;


for (let i = 0; i < images.length; i++) {
    i < 3 ? images[i].style.zIndex = '1' : images[i].style.zIndex = '2';        // 0-2 СОУС ЗНИЗУ -  zIndex '1', 2-9 ТОПІНГИ - zIndex '2'
}
basis.style.zIndex = '0' 


let arrPrice = [{ "mini_pizza": "small", "price": "20" },   // json данних усіх компонентів
                { "medium__pizza": "medium", "price": "25" },
                { "big_pizza": "big", "price": "30" },
                { "sauceClassic": "ketchup", "price": "5", "id": "sauceClassic", "name": "Кетчуп" },
                { "BBQ": "bbq", "price": "5", "id": "sauceBBQ", 'name': "BBQ" },
                { "sauceRikotta": "rikotta", "price": "5", "id": "sauceRikotta", 'name': "Рiкотта" },
                { "ordinary_cheese": "ordinary", "price": "10", "id": "moc1", 'name': "Сир звичайний" },
                { "feta_cheese": "feta", "price": "20", "id": "moc2", 'name': "Сир фета" },
                { "mozarella_cheese": "mozarella", "price": "20", "id": "moc3", 'name': "Моцарелла" },
                { "ham_beef": "ham", "price": "25", "id": "telya", 'name': "Телятина" },
                { "tomatoes": "tomat", "price": "5", "id": "vetch1", 'name': "Помiдори" },
                { "mushroom": "mushroom", "price": "5", "id": "vetch2", 'name': "Гриби" }
]


function priceBasisPizza() { 
    for (let j = 0; j < radio.length; j++) {
        pricePizza.innerText = " "   // очищувати поле від старої ціни 
        if (radio[j].checked) {
            priceBasis = +arrPrice[j].price;
        }
    }
    pricePizza.innerText = additivesPrice + priceBasis // ціна коржа + начинок
    totalPrice.appendChild(pricePizza) // вивести суму
}

priceBasisPizza()

for (let j = 0; j < radio.length; j++) {
    radio[j].addEventListener('click', priceBasisPizza) // запускати фунцію при зміні значення розміру піцци 
}



for (let i = 0; i < images.length; i++) {
    images[i].addEventListener('dragstart', function (evt) { // dragstart — происходит, когда пользователь начинает перетаскивать элемент.
        evt.dataTransfer.setData("srcId", evt.target.id);
        // console.log('dragStat evt')    
    })
}

dropTarget.addEventListener('dragover', function (evt) {
    evt.preventDefault();
    // console.log(evt)
})


dropTarget.addEventListener('drop', function (evt) {
    evt.preventDefault();
    let target = evt.target;
    let srcId = evt.dataTransfer.getData('srcId'); // ID img которое добавляем
    let droppable = target.classList.contains('table');
    let component = document.getElementById(srcId); // компонент який хочемо добавити 
    let newComponent = component.cloneNode(true) // його копія

    
    if (!toppings.includes(component) && !sauces.includes(component)) { // якщо немаэ такого компоненту, то...

        dropTarget.appendChild(newComponent)

        for (let i = 0; i < arrPrice.length; i++) {  // перебрати масив обэктів і задати значення "ціна, назва" для нового компоненту
            if (arrPrice[i].id == newComponent.id) {
                additivesPrice += +arrPrice[i].price
                newComponent.name = arrPrice[i].name
                newComponent.price = +arrPrice[i].price
            }

        }

        if (srcId == 'sauceClassic' || srcId == 'sauceBBQ' || srcId == 'sauceRikotta') {  // якщо це соус - до соусів 
            sauces.push(newComponent )

            let newSauce = document.createElement('li')  //  список компонентів 
            newSauce.textContent = newComponent.name
            saucess.append(newSauce)
            
            newSauce.addEventListener('click', () => { // удалити компонент при бажанні 
                newComponent.remove()
                newSauce.remove()
                additivesPrice -= newComponent.price
                pricePizza.innerText = additivesPrice + priceBasis;
            })

        }
        else {
            toppings.push(newComponent )
            let newTopping = document.createElement('li')
            newTopping.textContent = newComponent.name
            tosping.append(newTopping)

            newTopping.addEventListener('click', () => {
                newComponent.remove()
                newTopping.remove()
                additivesPrice -= newComponent.price
                pricePizza.innerText = additivesPrice + priceBasis;
            })

        }
        pricePizza.innerText = additivesPrice + priceBasis;

    }
    else (
        alert("You have already pushed this component")
    )



})


let runButtonLeft, runButtonTop, maxRunButtoneft, maxRunButtonTop;
let isProcessing = false

maxRunButtonRight = document.documentElement.clientWidth - runButton.offsetWidth;
maxRunButtonBottom = document.documentElement.clientHeight - runButton.offsetHeight;
runButton.addEventListener('mousemove', handler) 

function handler() { // дать шанс получить скидку для самых ловких ))) 
    if (!isProcessing) {        
        isProcessing = true
        setTimeout(
        function handler1 () {
            runButton.style.position = "absolute"
            runButtonRight = Math.random() * maxRunButtonRight;
            runButton.style.right = runButtonRight + 'px';
            runButtonBottom = Math.random() * maxRunButtonBottom;
            runButton.style.bottom = runButtonBottom + 'px';    
            isProcessing = false
            

        }, 100)
    }
  
}





document.getElementsByClassName('button')[1].addEventListener('click', () => {
       
    
        let isValid = true;
    
        const   name = document.getElementsByName("name")[0],
                phone = document.getElementsByName("phone")[0],
                email = document.getElementsByName("email")[0];  
    
        
        if (name.value.length == 0 || phone.value.length == 0 || email.value.length == 0 ) {
            isValid = false;
        }
       
        if (!isValid) {
    
            alert("Вcе поля вводов должны быть заполнены");
        }

        else {

            
            window.location.href = 'thank-you.html';

        }


})