

        function Cell(x, y) {
            this.opened = false;
            this.bombPresence = false;
            this.flagInstalled = false;
            this.isOpened = false;
            this.nearBombCounter = 0;
            this.x = x;
            this.y = y
        }

        class GameStructure {
            constructor(height, width, requiredBombs) {
                this.height = height;
                this.width = width;
                this.bombsCount = requiredBombs;
                this.gameField = new Array(height).fill(0).map(el => new Array(width).fill(0));
                this.status = 'init' //loose, win, inprogress
                this.createGamingField() // сразу создать поле
                this.createRandomBomb() // сразу сгенерировать бомбы
                this.counterBombs() // и посчитать число соседних бомб 
            }

            createGamingField() {                 // заполнить поле ячейками new Cell
                for (let x = 0; x < this.width; x++) {
                    for (let y = 0; y < this.height; y++) {
                        this.gameField[y][x] = new Cell(x, y)
                    }
                }
                return this.gameField
            }

            createRandomBomb() {      // создать бомбы        
                let bombsCounter = 0
                while(bombsCounter <  this.bombsCount) {         // пока bombsCounter не будет равен this.bombsCount случайно генерировать бомбы
                    for (let x = 0; x < this.width; x++) {
                        for (let y = 0; y < this.height; y++) {
                            if (bombsCounter < this.bombsCount && !this.gameField[y][x].bombPresence) { // !this.gameField[y][x].bombPresence избежать генерации бомбы в одну ячейку
                                let bombRandom = Math.random() >= 0.9;
                                if (bombRandom == true ) {
                                    this.gameField[y][x].bombPresence = true // после генерации бомбы изменить статус bombPresence = true 
                                    bombsCounter++
                                }
                            }
                        }
                    }
                }
            }


            getNearestCell(y, x) {  //  найти всех соседов
                const mapNearestCellsCoordinate = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, +1], [+1, -1], [+1, 0], [+1, +1]]; // массив координат всех соседей ячейки [y][x]
                return mapNearestCellsCoordinate.map(el => {
                    const newY = y + el[0]
                    const newX = x + el[1]

                    if (newY < this.height && newX < this.width && newY >= 0 && newX >= 0) // получить массив всех соседов ячейки [y][x]
                        return this.gameField[newY][newX]
                }).filter(el => el != undefined)         // выбрать только соседей  Cell ячейки [y][x]

            }


            counterBombs() { // посчитать количество бомб рядом с  ячейкой
                for (let x = 0; x < this.width; x++) {
                    for (let y = 0; y < this.height; y++) {
                        const nearestCell = this.getNearestCell(y, x)  // массив всех соседей Cell ячейки [y][x]

                        const bombs = nearestCell.reduce((sum, el) =>
                            sum + +el.bombPresence, 0)              // посчитать соседние бомбы ячейки [y][x]
                        this.gameField[y][x].nearBombCounter = bombs // записать в nearBombCounter количество соседних бомб 

                    }
                }
            }


            setFlag(y, x) { // раставить или убрать флажки 
                if (this.status == 'init') { // после первого флажка статус сменить, чтобы вывести кнопку "Начать сначала"
                    this.status = 'inprogress'
                }

                if (this.gameField[y][x].flagInstalled == false) {  // если флаг не стоит - поставить
                    return this.gameField[y][x].flagInstalled = true
                }
                else {
                    return this.gameField[y][x].flagInstalled = false // и наоборот
                }
            }


            openedCell(y, x) { // открыть все ячейки, которые по соседству не имеют бомб
                if (this.status == 'init') { // после первой открытой ячейки статус сменить, чтобы вывести кнопку "Начать сначала"
                    this.status = 'inprogress'
                }

                if (!this.gameField[y][x].isOpened && !this.gameField[y][x].bombPresence) {  // если не открыта и нет бомбы - открыть 
                    this.gameField[y][x].isOpened = true
                    const nearestCell = this.getNearestCell(y, x)  // запустить рекурсвную функцию для соседних ячеек, чтобы проверить все соседние ячейки 
                    if (this.gameField[y][x].nearBombCounter === 0) {
                        nearestCell.forEach(el => this.openedCell(el.y, el.x))   
                    }
                }

                else if (this.gameField[y][x].bombPresence) {  // если бомба открыта - loose, проиграш
                    this.gameField[y][x].isOpened = true
                    this.status = 'loose'
                }
            }


            flagCounter() { // количество поставленных флагов
               let counterFlag = 0
                for (let x = 0; x < this.width; x++) {
                    for (let y = 0; y < this.height; y++) {
                        if (this.gameField[y][x].flagInstalled == true) { //  counterFlag количество всех флаговб которое выводится
                            counterFlag++
                        }
                    }
                }
                return counterFlag
            }


            checkGameWin() {             //  условия победы: поставленны 1. флаги на все бомбы   2. открыты все ячейки
                let statusOpenCell = true;
                let counterFlagPlusBomb = 0;
                for (let x = 0; x < this.width; x++) {
                    for (let y = 0; y < this.height; y++) {
                        if (this.gameField[y][x].flagInstalled && this.gameField[y][x].bombPresence) {  // счетчик  флаг + бомба 
                        counterFlagPlusBomb++
                        }

                        if (!this.gameField[y][x].isOpened && !this.gameField[y][x].bombPresence) { // если хоть одна ячейка не открыта статус  false
                            statusOpenCell = false
                        }  

                    }
                }


                if (counterFlagPlusBomb == this.bombsCount || statusOpenCell) {   // если счетчик  флаги на все бомбы  равен количеству бомб и все ячейки открыты
                    this.status == 'win' 
                    alert (" WINNNNNNN! you are amasing. Restart new game ")
                    
                }
                 return this.status
            }
    }


// --------------------------------UI ---------------------------------------------//
        const container = document.getElementById('app-container')
        const flagsCounter = document.getElementById('flag-counter')
        flagsCounter.style.marginTop = '10px'
        

        function renderGame(game) { 

            container.innerHTML = ''  // контейнер чистится
           
            flagsCounter.innerText = ` You already add ${game.flagCounter() } `   // вывод чиста поставленных флагов
            const img = document.createElement('img')
            img.src = './img/flag1.png'
            flagsCounter.appendChild(img).style.position = 'absolute'
            game.checkGameWin() 


            if ( game.status == 'loose' || game.status == 'win' || game.status == 'inprogress') { // вывод после первого действия кнопки Restart new game 
                const buttonRestartGame = document.createElement('button')
                buttonRestartGame.addEventListener('click', (event) => {  // после клика создать новую игру
                renderGame(new GameStructure(8, 7, 10))
                })

                container.appendChild(buttonRestartGame )
                buttonRestartGame.innerHTML = 'Restart new game'
                buttonRestartGame.style.width = container.style.width
                buttonRestartGame.style.marginBottom = '10px'
            }
            
            container.style = `
                display: flex;
                width: ${game.width * 25}px;
                flex-wrap: wrap;
            `
                
            for (let y = 0; y < game.height; y++) {
                for (let x = 0; x < game.width; x++) {
                    const gameCell = game.gameField[y][x]
                    const cell = document.createElement('div') // создать 56 (height * width ) ячеек 
                    
                    if ( game.status == 'inprogress' || game.status == 'init' || game.status == 'win'  ) {  // если не loose создавать событие и дедать ход
                        cell.addEventListener('contextmenu', (event) => { 
                            event.preventDefault()  // убрать стандартное окно при нажатии на правую кнопку 
                            game.setFlag(y, x) // ставить или убирать флаг
                            renderGame(game) // рендерить "типа заново" поле из новыми статусами и обьктами 
                        })

                        cell.addEventListener('click', () => { // открыть ячейку
                            game.openedCell(y, x) 
                            renderGame(game)
                        })

                    }

                    // стиль ячейки
                    cell.style = `  
                        width: 25px;
                        height: 25px;
                        background-color: #87CEEB;
                        box-sizing: border-box;
                        border: 1px solid #808080;
                    `
                   
                       

                    if (game.status == 'loose' && gameCell.bombPresence) { // если програл и бомба есть = нарисовать ее
                        const bomb = document.createElement('img')
                        bomb.src = './img/mine.png'
                        cell.appendChild(bomb)
                    }

                    else {
                        if (gameCell.flagInstalled) {               // нарисовать флаг
                            const img = document.createElement('img')
                            img.src = './img/flag.png'
                            cell.appendChild(img)
                            cell.style.backgroundColor = '#F5F5DC'
                    }

                        if (gameCell.isOpened) {  // поменять цвет открытой ячейки
                            cell.innerText = gameCell.nearBombCounter
                            cell.style.backgroundColor = '#C0C0C0'
                        }
                    }
               
                    container.appendChild(cell) 
                }
            }
        }

    
        let newGame = new GameStructure(8, 7, 10)
        renderGame(newGame)
        console.log(newGame)











